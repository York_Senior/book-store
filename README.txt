This file describes how the web application and ontology can be run.

Download/Clone the Git Repository

Ontology:
        - Download Protege ontology editor
        - Navigate to *\book-store\Ontology\ and open the ThesisOntology.owl file
        
        * Please note that only the requirements specified in this ontology have been implemented in the web application.
        ** The relevant GitCommits on the 10-Features can help idenify where these features are within this source code.

Web application:
        
        - Download and install Docker desktop from: https://www.docker.com/products/docker-desktop
        Open the program and ensure that it is running.
        
        - open the terminal and cd into: *\book-store\store\
        
        - Here the following files should be present:
        Mode                LastWriteTime         Length Name                                                                                
        ----                -------------         ------ ----                                                                                
        d-----       08/01/2020     12:59                media
        d-----       08/01/2020     12:59                store
        d-----       08/01/2020     12:59                webstore
        -a----       05/02/2020     17:21         221184 db.sqlite3
        -a----       08/01/2020     12:59            222 docker-compose.yml
        -a----       08/01/2020     12:59            151 Dockerfile
        -a----       08/01/2020     12:59            646 manage.py
        -a----       08/01/2020     12:59             67 requirements.txt
        
        - Make sure docker is running.
        - run the command: docker-compose build
        - run the command: docker-compose up
        - Open your web browser and navigate to: http://localhost:8080/
