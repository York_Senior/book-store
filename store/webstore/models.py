from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.
class Book(models.Model):
    
    name = models.CharField(
        max_length = 255,
    )
    
    author = models.CharField(
        max_length = 255,
    )

    year = models.IntegerField(
        validators=[
            MaxValueValidator(3000),
            MinValueValidator(1500)
        ]
    )

    price = models.DecimalField (
        max_digits=5,
        decimal_places=2,
    )

    genreChoices = (
    ('chilren', 'Chilren'),
    ('crime', 'Crime'),
    ('biography', 'Biography'),
    ('business', 'Business'),
    ('fiction', 'Fiction'),
    ('history', 'History'),
    )
    genre = models.CharField(
        max_length = 32,
        choices = genreChoices,
    )

    description = models.CharField(
        max_length = 2040,
        null=True,
    )

    image = models.ImageField(
        blank=True,
        null=True, 
        upload_to= 'book_covers'
    )

class Profile(models.Model):
    #Djangos user model includes:
    # first_name, last_name, username, email, password, permissions, groups, is_active, is_staff, date_joined & last_login 
    user = models.OneToOneField(
        User,
        on_delete = models.CASCADE,
        unique=True,
        default=None,
        null=True,
    )
    User.profile = property(lambda u: Profile.objects.get_or_create(user=u)[0])
    
    genderChoices = (
        ('Male', 'Male'),
        ('Female', 'Female')
    )

    gender = models.CharField(
        max_length=6,
        choices=genderChoices,
        null=True,
        blank=True,
    )

    dateOfBirth = models.DateField(
        null=True,
        blank=True,
    )

    profile_picture = models.ImageField(
        default= 'default.png',
        blank=True,
        null=True, 
        upload_to= 'profile_pictures',
    )

    acceptPrivacyPolicyandTermsandConditions = models.BooleanField(
        null=False,
        default=False,
        help_text="Please see terms and conditions and policy at the bottom of this page.  This must be selected to use this service."
    )   

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
        Address.objects.create (user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
    instance.address.save()

class Address(models.Model):
    user = models.OneToOneField(
        User,
        on_delete = models.CASCADE,
        default=None,
        null=True,
    )
    street_address = models.CharField(
        max_length=100,
        null=True,
        blank=True,
    )

    apartment_address = models.CharField(
        max_length=100,
        null=True,
        blank=True,
    )

    country = models.CharField(
        max_length = 255,
        null=True,
        blank=True,
    )

    zip = models.CharField(
        max_length=100,
        null=True,
        blank=True,
    )
    

class CreditCard(models.Model):
    user = models.ForeignKey(
        User,
        on_delete = models.CASCADE,
        null=True,
    )

#this should be a temporary storage (each user only has one and they can only access that one particular cart). 
#each time an order is created, the cart is DELETED after the order is created:) (IF a cart exists, then add to current )

class ShoppingCart(models.Model):
    user = models.ForeignKey(
        User,
        on_delete = models.CASCADE,
        default=None,
        null=True,
    )
    #create a list of items that should be added to an array You need to change this to an array
    #Look at making a django Many-Many field (Or many to one field)

    book = models.ForeignKey(
        Book,
        on_delete = models.CASCADE,
        default=None,
        null=True,
    )

class Order(models.Model):
    user = models.ForeignKey(
        User,
        on_delete = models.CASCADE,
        null=True,
    )




 
