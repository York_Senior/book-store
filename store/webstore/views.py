from django.shortcuts import render
from django.template import loader
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, get_user_model, login, logout, models, views as auth_views
from django.contrib.auth.models import User, Group, Permission
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.core.exceptions import PermissionDenied
from django.views.generic.edit import CreateView
from django.contrib import messages


from .models import *
from .forms import *


#All users
def home_page(request):
    
    title = "Home page"
    return render(request, 'home_page.html')

def privacy_policy(request):
    #Change to make this a PDF :)
    title = "Privacy Policy"
    return render(request, 'privacy_policy.html')

#All users
def browse_books(request):
        title = "All books"
        all_books = Book.objects.all()
        context = {
        "all_books" : all_books,
        "title": title,
        }
        return render(request, 'browse_books.html', context )

@login_required(login_url = 'login') #Only logged in users
def book_detail(request, Book_id):
    title = "Book Detail"
    book = Book.objects.get(pk=Book_id)
    context = {
        "book" : book,
        "title":title,
    }
    return render(request, 'book_detail.html', context)

def client_register(request):
    title = "Register"
    if request.user.is_authenticated:
            return redirect('home_page')
    if request.method == "POST":
        form1 = UserRegisterForm(request.POST)
        context = {
        'form1': form1,
        "title": title,
        }
        if form1.is_valid():
            new_user = User.objects.create_user(**form1.cleaned_data)
            new_u = Group.objects.get(name = 'client')
            new_u.user_set.add(new_user)
            return HttpResponseRedirect('registerProfile')
    else:
        form1 = UserRegisterForm()
        context = {
        'form1': form1,
        "title": title,
        }
    #returns user to the register form
    return render(request, 'register.html', context)

@login_required(login_url = 'login')#Only logged in users
def profile_register(request):
    profile = request.user.profile
    address = request.user.address
    title = "Next"
    if request.method == "POST":
        form1 = ProfileRegisterForm(request.POST, request.FILES, instance=profile)
        form2 = AddressRegisterForm(request.POST, request.FILES, instance=address)
        context = {
        'form1' : form1,
        'form2' : form2,
        "title": title,
        }
        if form1.is_valid() and form2.is_valid():
            form1.save(form1.cleaned_data)
            form2.save(form2.cleaned_data)
            return HttpResponseRedirect('userProfile')
    else:
        form1 = ProfileRegisterForm(request.POST, request.FILES, instance=profile)
        form2 = AddressRegisterForm(request.POST, request.FILES, instance=address)
        context = {
        'form1': form1,
        'form2': form2,
        "title": title,
        }
    #returns user to the register form
    return render(request, 'register.html', context)

@login_required(login_url = 'login')#Only logged in users
def user_profile_update(request):
    profile = request.user.profile
    address = request.user.address
    user = request.user
    form1 = UserUpdateForm(request.POST, instance=user)
    form2 = ProfileUpdateForm(request.POST, request.FILES, instance=profile)
    form3 = AddressUpdateForm(request.POST, request.FILES, instance=address)
    title = "Update"
    if request.method == "POST":
        form1 = UserUpdateForm(request.POST, instance=user)
        form2 = ProfileUpdateForm(request.POST, request.FILES, instance=profile)
        form3 = AddressUpdateForm(request.POST, request.FILES, instance=address)
        context = {
        'form1' : form1,
        'form2' : form2,
        'form3' : form3,
        "title": title,
        }
        if form1.is_valid() and form2.is_valid() and form3.is_valid():
            form1.save(form1.cleaned_data)
            form2.save(form2.cleaned_data)
            form3.save(form3.cleaned_data)
            return HttpResponseRedirect('userProfile')
    else:
        form1 = UserUpdateForm(request.POST, instance=user)
        form2 = ProfileUpdateForm(request.POST, request.FILES, instance=profile)
        form3 = AddressUpdateForm(request.POST, request.FILES, instance=address)
        context = {
        'form1' : form1,
        'form2' : form2,
        'form3' : form3,
        "title": title,
        }
    #returns user to the register form
    return render(request, 'update_profile.html', context)

def login_view(request):
    next = request.GET.get('next')
    title = "Login"
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        username = request.POST.get("username")
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        login(request, user)
        if request.GET.get('next', None):
            return HttpResponseRedirect (request.GET['next'])
        return redirect('home_page')
    return render(request, "login.html", {"form":form, "title": title})

@login_required(login_url = 'login')#Only logged in users
def user_profile(request):
    profile = request.user.profile
    address = request.user.address
    title = 'User Profile'
    context = {
        'title' : title,
        'user' : request.user,
        'profile' : profile,
        'address' : address,
        }
    return render(request, 'user_profile.html', context)

@login_required(login_url = 'login')
def logout_view(request):
    logout(request)
    return redirect('home_page')

@login_required(login_url = 'login')
def delete_view(request):
    context = {}
    user = request.user
    if request.method == "POST":
        user.delete()
        return redirect('home_page')