from django.urls import path
from . import views
from django.contrib.staticfiles.urls import static
from django.conf import settings



urlpatterns = [
    path('', views.home_page, name='home_page'),
    path('privacyPolicy', views.privacy_policy, name='privacy_policy'),
    path('browse', views.browse_books, name='browse_books'),
    path('browse/<int:Book_id>/', views.book_detail, name='book_detail'),
    path('register', views.client_register, name='register'),
    path('registerProfile', views.profile_register, name='registerProfile'),
    path('login', views.login_view, name='login'),
    path('logout', views.logout_view, name='logout'),
    path('userProfile', views.user_profile, name='user_profile'),
    path('userProfileUpdate', views.user_profile_update, name='user_profile_update'),
    path('userAccountDelete', views.delete_view, name='user_account_delete')


]  + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)   