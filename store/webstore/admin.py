from django.contrib import admin
from .models import Book, Profile, Address, ShoppingCart
# Register your models here.


class BookAdmin(admin.ModelAdmin):
    list_display = ['name','author']

admin.site.register(Book,BookAdmin)

class ProfileAdmin(admin.ModelAdmin):
    list_display = ['user','gender', 'dateOfBirth', 'profile_picture','acceptPrivacyPolicyandTermsandConditions']

admin.site.register(Profile,ProfileAdmin)

class AddressAdmin(admin.ModelAdmin):
    list_display = ['user','street_address', 'apartment_address', 'country', 'zip']

admin.site.register(Address,AddressAdmin)

class ShoppingCartAdmin(admin.ModelAdmin):
    list_display = ['user', 'book']

admin.site.register(ShoppingCart,ShoppingCartAdmin)