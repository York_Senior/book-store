# Generated by Django 2.2.5 on 2019-12-19 15:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webstore', '0016_auto_20191219_1538'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='dateOfBirth',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='gender',
            field=models.CharField(blank=True, choices=[('M', 'Male'), ('F', 'Female')], max_length=1, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='profile_picture',
            field=models.ImageField(blank=True, default='default.jpg', null=True, upload_to='profile_pictures'),
        ),
    ]
