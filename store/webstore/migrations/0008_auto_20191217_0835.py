# Generated by Django 2.2.5 on 2019-12-17 08:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webstore', '0007_book_price'),
    ]

    operations = [
        migrations.AlterField(
            model_name='address',
            name='id',
            field=models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
    ]
