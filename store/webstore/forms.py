from django import forms
from django.contrib.auth.models import User, Group
from .models import *
from django.contrib.auth import (
    authenticate,
    get_user_model,
    login,
    logout,
    )



User = get_user_model()

class UserRegisterForm(forms.ModelForm):
    email = forms.EmailField(label='Email address')
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'first_name',
            'last_name',
            'password',
        ]
class ProfileRegisterForm(forms.ModelForm):
        class Meta:
            model = Profile
            fields = [
                'dateOfBirth',
                'gender',
                'profile_picture',
                'acceptPrivacyPolicyandTermsandConditions',
            ]
            acceptPrivacyPolicyandTermsandConditions = forms.BooleanField(initial=True,required=False)

class AddressRegisterForm(forms.ModelForm):
        class Meta:
            model = Address
            fields = [
               'street_address',
                'apartment_address',
                'country',
                'zip',
            ]

class ProfileUpdateForm(forms.ModelForm):
        class Meta:
            model = Profile
            fields = [
                'dateOfBirth',
                'gender',
                'profile_picture',
            ]

class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField(label='Email address')
    class Meta:
        model = User
        fields = [
            'email',
            'first_name',
            'last_name',
        ]

class AddressUpdateForm(forms.ModelForm):
        class Meta:
            model = Address
            fields = [
               'street_address',
                'apartment_address',
                'country',
                'zip',
            ]

class UserLoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get("username")
        password = self.cleaned_data.get("password")
        if username and password:
            user = authenticate(username=username, password=password)
            if not user:
                raise forms.ValidationError("This user does not exist")
            if not user.check_password(password):
                raise forms.ValidationError("Incorrect passsword")
        return super(UserLoginForm, self).clean(*args, **kwargs)